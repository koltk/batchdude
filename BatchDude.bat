@echo off

REM By Kolto101
REM Project began Jun 18, 2011. Dead after July 2013. Revived Aug 2018.

REM Solutions for classic mode taken from:
REM https://www.detachedsolutions.com/puzzpack/blockdude.php

REM TODO: Change title graphics to reflect config?

:: Accessor for editor [or other external files]
if NOT "%~1" == "" (
	if /i "%~1" == "record" (
		call :load_map "%~2" "record"
	) ELSE if /i "%~1" == "play_solution" (
		call :load_map "%~2" "play_solution"
	)
	if NOT "%~1" == "start" exit /b !ERRORLEVEL!
)

if "%~1" == "start" goto main
:start_loop
	cmd /V:ON /Q /c "BatchDude.bat start"
	echo.
	if "%ERRORLEVEL%" == "0" (
		echo Script finished successfully.
	) ELSE (
		echo Script crashed.
		echo.
		echo ERRORLEVEL: %ERRORLEVEL%
		echo.
		echo If the game is unmodified, please report the incident to:
		echo kolto101@gmail.com
	)
	:run_again_loop
		echo.
		set /p _input=Run again? [y/n]: 
		if /i "%_input%" == "y" goto start_loop
		if /i "%_input%" == "n" EXIT
goto run_again_loop

:main

call BatchDudeConfig.bat "init"
setlocal EnableDelayedExpansion

:menu
	title BatchDude
	mode 80,30
	cls
	echo.
	echo  컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴{ Kolto101 Presents }컴컴컴컴컴컴컴컴컴컴컴컴컴�
	echo.
	echo    �袴袴袴袴袴袴袴袴袴袴敲           _ __                 ___
	echo    � 1. Classic          �          ( /  )     _/_    /  ( / \       /
	echo    勁袴袴袴袴袴袴袴袴袴袴�           /--^< __,  /  _, /_   /  /, , __/ _
	echo    � 2. Load Map         �          /___/(_/(_(__(__/ / (/\_/(_/_(_/_(/_
	echo    勁袴袴袴袴袴袴袴袴袴袴�
	echo    � 3. Map Editor       �                --A Block Dude tribute--
	echo    勁袴袴袴袴袴袴袴袴袴袴�
	echo    � 4. Play Solution    �
	echo    勁袴袴袴袴袴袴袴袴袴袴�
	echo    � 5. Tutorial         �                  �                  �
	echo    勁袴袴袴袴袴袴袴袴袴袴�                  �                  �
	echo    � 6. Configure        �                  �                  �
	echo    勁袴袴袴袴袴袴袴袴袴袴�                  �                  �
	echo    � 7. About            �                  �   �       �      �
	echo    勁袴袴袴袴袴袴袴袴袴袴�                  �  �   � @ � @   �
	echo    � 8. Exit             �                  栢栢栢栢栢栢栢栢栢栢
	echo    훤袴袴袴袴袴袴袴袴袴袴�
	echo.
	set /p _input=Choose a number: 
	     if "%_input%" == "1" (call :classic_start "1")^
	ELSE if "%_input%" == "#" (call :enter_password || call :classic_start "!ERRORLEVEL!")^

	ELSE if "%_input%" == "2" (call :load_map)^
	ELSE if "%_input%" == "3" (call BatchDudeEditor.bat "start")^
	ELSE if "%_input%" == "4" (call :load_map "" "play_solution")^
	ELSE if "%_input%" == "5" (call :howtoplay)^
	ELSE if "%_input%" == "6" (call BatchDudeConfig.bat)^
	ELSE if "%_input%" == "7" (call :about)^
	ELSE if "%_input%" == "8"  EXIT /b 0
goto menu


:enter_password
	echo.
	set /p _input=Enter 3 letter case sensitive password [or enter for level 1]: 
	set _level=0
	REM Note: these are case sensitive, so no /i
	if "%_input%" == "tcP" set _level=1
	if "%_input%" == "ARo" set _level=2
	if "%_input%" == "CKs" set _level=3
	if "%_input%" == "daN" set _level=4
	if "%_input%" == "BAH" set _level=5
	if "%_input%" == "Ion" set _level=6
	if "%_input%" == "Twe" set _level=7
	if "%_input%" == "nTy" set _level=8
	if "%_input%" == "iRC" set _level=9
	if "%_input%" == "JmK" set _level=10
	if "%_input%" == "wTF" set _level=11
	if "%_level%" == "0" (
		echo !LF!Invalid password or none entered.!LF!
	)
exit /b !_level!

:get_password levelIndex
	if "%~1" == "1"  set $_pass=tcP
	if "%~1" == "2"  set $_pass=ARo
	if "%~1" == "3"  set $_pass=CKs
	if "%~1" == "4"  set $_pass=daN
	if "%~1" == "5"  set $_pass=BAH
	if "%~1" == "6"  set $_pass=Ion
	if "%~1" == "7"  set $_pass=Twe
	if "%~1" == "8"  set $_pass=nTy
	if "%~1" == "9"  set $_pass=iRC
	if "%~1" == "10" set $_pass=JmK
	if "%~1" == "11" set $_pass=wTF
exit /b

:classic_start level

	set game.classic=1
	set levelIndex=1
	call :enter_password || set levelIndex=!ERRORLEVEL!

	:classic_start_sub1
		call :get_password "!levelIndex!"
		echo.
		echo Level !levelIndex!
		echo Password: !$_pass!
		echo.
		pause
		call :load_map "Classic\level%levelIndex%" "classic" || (
			if "!ERRORLEVEL!" == "4" (
				if "!levelIndex!" == "11" (
					echo.
					echo Congratulations^^! You've beaten all 11 levels of BatchDude.
					echo.
					pause>nul
				) ELSE (
					set /a levelIndex+=1
					goto classic_start_sub1
				)
			)
		)
	set game.classic=
exit /b

:clearAllVars
	for %%a in (bdc. game. _) do (
		(set %%a> nul 2>&1) && (
			for /f "tokens=1 delims==" %%b in ('set %%a') do (
				REM Need to use the quote syntax, otherwise we'll set it as a space!
				set "%%b=" >nul 2>&1
			)
	))
exit /b

:load_map

    if NOT "%~1" == "" (
    	set _input=%~1
    	goto load_map_sub2
    )
	set _maxFiles=0
	set NUM_SHOW_FILES=10

	if /i "%cheats.showClassicLevels%" == "ON " (
		for /l %%c in (1,1,11) do (
			set /a _maxFiles+=1
			set _fname.!_maxFiles!=Classic\level%%c
		)
	)

	for /f "tokens=*" %%f in ('dir /b /a-d /on "MAPS\"') do (
		set /a _maxFiles+=1
		set _fname.!_maxFiles!=%%~nf
	)
	set _mStart=1
	set _mEnd=!NUM_SHOW_FILES!

	:load_map_sub1
		cls
		echo.
		if !_mStart! LSS 1 (
			set _mStart=1
			set _mEnd=!NUM_SHOW_FILES!
		)
		if !_mStart! GTR !_maxFiles! (
			set /a _mEnd-=!NUM_SHOW_FILES!
			set /a _mStart-=!NUM_SHOW_FILES!
		)
		for /l %%m in (!_mStart!,1,!_mEnd!) do (
			if defined _fname.%%m (echo  %%m. !_fname.%%m!) ELSE echo.
		)
		echo.
		if !_maxFiles! GTR !NUM_SHOW_FILES! (
			echo  [b] Back   [w] Scroll up   [s] Scroll down
		) ELSE echo  [b] Back
		echo.
		set /p _input=Enter: 
		if /i "!_input!" == "b" exit /b
		if /i "!_input!" == "w" (
			set /a _mStart-=!NUM_SHOW_FILES!
			set /a _mEnd-=!NUM_SHOW_FILES!
			goto :load_map_sub1

		) ELSE 	if /i "!_input!" == "s" (
			set /a _mStart+=!NUM_SHOW_FILES!
			set /a _mEnd+=!NUM_SHOW_FILES!
			goto :load_map_sub1

		) ELSE if !_input! GEQ 1 if !_input! LEQ !_maxFiles! set _input=!_fname.%_input%!

	:load_map_sub2
	if /i exist "MAPS\!_input!"  set _input=!_input:.map=!
	if /i NOT exist "MAPS\!_input!.map" goto :load_map_sub1

	call :init "!_input!" "%~2"

exit /b !ERRORLEVEL!


:init

	call :clearAllVars
	set game.direction=-
	set game.moveCounter=0
	set game.hasBlock=false

	for /f "tokens=1* delims==" %%a in (MAPS\%~1.map) do (
		set %%a=%%b
		if defined config.graphics.%%b set %%a=!config.graphics.%%b!
	)

	REM Fill undefined variables with spaces
	for /l %%r in (1,1,!bdc.mapHeight!) do (
		for /l %%c in (1,1,!bdc.mapWidth!) do (
			if not defined bdc.%%c.%%r set bdc.%%c.%%r=!config.graphics.space!
	))


	REM Build the display screen and store it as a variable.
	set game.disp=
	for /l %%h in (1,1,%bdc.mapHeight%) do (
		for /l %%w in (1,1,%bdc.mapWidth%) do set game.disp=!game.disp!!bdc.%%w.%%h!
		set game.disp=!game.disp!!LF!
	)

	if defined bdc.playerX set game.playerX=!bdc.playerX!
	if defined bdc.playerY set game.playerY=!bdc.playerY!

	set game.prevPlayerX=!game.playerX!
	set game.prevPlayerY=!game.playerY!
	set /a game.prevBlockY=!game.playerY! - 1
	if !game.prevBlockY! LSS 1 set game.prevBlockY=!bdc.mapHeight!
	set game.prevPlayerLocSym=!bdc.%game.playerX%.%game.playerY%!
	set game.prevBlockLocSym=!bdc.%game.playerX%.%game.prevBlockY%!

	set game.dispTitle=
	if defined bdc.mapname (
		set game.dispTitle=!bdc.mapname!
		if defined bdc.author set game.dispTitle=!bdc.mapname! by !bdc.author!
	) ELSE if defined bdc.author set game.dispTitle=By !bdc.author!

	call :updatePosition

	set _ctrlStr=[%config.input:~2,1%] Left   [%config.input:~3,1%] Right   [%config.input:~1,1%] Up   [%config.input:~0,1%] Block   [%config.input:~4,1%] Menu
	set game.input=choice /c:%config.input% /n /M "%_ctrlStr%    >"
	set game.recordInput=
	set game.playbackStr=
	set $_playback=
	if /i "%~2" == "play_solution" (
		if defined bdc.solution (
			set game.playbackStr=!bdc.solution!
			set "game.input=call :eval_playback && exit /b"
			call :game_loop
		) ELSE (
			echo.
			echo ERROR: No solution defined for this level
			echo.
			pause
		)
		exit /b
	)

	if /i "%~2" == "record" (
		REM Turn off cheats for recording
		call BatchDudeConfig.bat "cheats_off"
		set game.recordInput=!game.input!
		set game.input=call :record_moves
		call :game_loop
		exit /b !ERRORLEVEL!
	)

	call :game_loop || (
		if "!ERRORLEVEL!" == "1" (
			REM Quit to menu
			exit /b 1
		) ELSE if "!ERRORLEVEL!" == "2" (
			REM Restart the level
			goto init
		) ELSE (
			echo.
			if "!ERRORLEVEL!" == "3" (
				REM Game over
				echo Game over^^!
			) ELSE if "!ERRORLEVEL!" == "4" (
				REM Win
				echo Level complete^^!
			)
			echo.
			echo Moves: !game.moveCounter!
			echo.
			if /i NOT "%~2" == "classic" (
				set /p _input=Play again? [y/n]: 
				if /i "!_input!" == "y" goto init
			) ELSE pause>nul
			exit /b !ERRORLEVEL!
		)
	)
exit /b 0

:testMap
	for /l %%b in (1,1,10) do set bdc.%%b.10=border
	for /l %%b in (1,1,10) do set bdc.%%b.10=border
	set bdc.5.10=space
	set bdc.5.5=border
	set bdc.7.9=block
	set bdc.8.9=block
	set game.playerX=5
	set game.playerY=4
	set bdc.mapHeight=10
	set bdc.mapWidth=10
exit /b

:display
	cls
	if defined game.dispTitle (
		echo.
		echo   %game.dispTitle%
		echo.
	)
	if "%debugmode%" == "On" (
		echo Dir: !game.direction!    Pos: !game.playerX!.!game.playerY!
	) ELSE (
		echo.
	)
	echo !game.disp!
exit /b

:game_loop
	call :display
	echo.
	%game.input%
		 if "!ERRORLEVEL!" == "1" (call :pickupOrPlaceBlock)^
	ELSE if "!ERRORLEVEL!" == "2" (call :moveUp                  || exit /b !ERRORLEVEL!)^
	ELSE if "!ERRORLEVEL!" == "3" (call :movePlayerLeftRight "-" || exit /b !ERRORLEVEL!)^
	ELSE if "!ERRORLEVEL!" == "4" (call :movePlayerLeftRight "+" || exit /b !ERRORLEVEL!)^
	ELSE if "!ERRORLEVEL!" == "5" (call :inGameMenu              || exit /b !ERRORLEVEL!)^
	ELSE if "!ERRORLEVEL!" == "6"  call :playerGravity           || exit /b !ERRORLEVEL!
goto :game_loop

:eval_playback

	REM Step every one second [or faster given user input]
	choice /c ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 /N /T 1 /D A /M "[M] Menu   [P] Pause   [any key] Fast forward"
	if "%ERRORLEVEL%" == "13" (
		exit /b 5
	) ELSE if "%ERRORLEVEL%" == "16" pause

	if NOT "%game.playbackStr%" == "" (
		set _next=%game.playbackStr:~0,1%
		set game.playbackStr=%game.playbackStr:~1%
		if /i "!_next!" == "b" (
			exit /b 1
		) ELSE if /i "!_next!" == "u" (
			exit /b 2
		) ELSE if /i "!_next!" == "-" (
			exit /b 3
		) ELSE if /i "!_next!" == "+" (
			exit /b 4
		)
	)
exit /b 0

:record_moves
	if /i "%game.moveCounter%" == "%game.lastMoveCounter%" (
	if /i "%game.direction%"   == "%game.lastdir%" (
	if /i "%game.playerX%"     == "%game.lastX%" (
	if /i "%game.hasBlock%"    == "%game.lastHasBlock%" (
		REM Theoretically we haven't done work, so don't record it.
		set $_playback=!$_playback:~0,-1!
	))))
	set game.lastMoveCounter=%game.moveCounter%
	set game.lastdir=%game.direction%
	set game.lastX=%game.playerX%
	set game.lastHasBlock=%game.hasBlock%
	REM echo recording !$_playback! ::: %game.moveCounter%
	%game.recordInput%
	if "!ERRORLEVEL!" == "1" (
	 	set $_playback=!$_playback!b
	) ELSE if "!ERRORLEVEL!" == "2" (
	 	set $_playback=!$_playback!u
	) ELSE if "!ERRORLEVEL!" == "3" (
		set $_playback=!$_playback!-
	) ELSE if "!ERRORLEVEL!" == "4" (
		set $_playback=!$_playback!+
	)
exit /b !ERRORLEVEL!

:movePlayerLeftRight directionKey
	call :changeDirection  "%~1" || exit /b 0
	call :check_and_move_x "%~1"
exit /b

:changeDirection direction
	if "!game.direction!" == "%~1" exit /b 0
	set game.direction=%~1
	REM call :changeAvatar "%~1"
	if /i "!cheats.sharpturn!" == "ON " exit /b 1
exit /b 0

:moveUp
	set /a _newX=!game.playerX! !game.direction! 1
	set /a _newY=!game.playerY! - 1

	if !_newX! GTR !bdc.mapWidth!  set _newX=1
	if !_newX! LSS 1               set _newX=!bdc.mapWidth!
	if !_newY! GTR !bdc.mapHeight! set _newY=1
	if !_newY! LSS 1               set _newY=!bdc.mapHeight!

	set /a _newBlockY=!_newY! - 1
	if !_newBlockY! LSS 1 set _newBlockY=!bdc.mapHeight!

	if /i NOT "!cheats.ghostdude!" == "ON " (
		REM Check if there is something to step on.
		if /i "!bdc.%_newX%.%game.playerY%!" == "!config.graphics.space!" exit /b 0

		REM check if there is space for the player in the desired location.
		if /i NOT "!bdc.%_newX%.%_newY%!" == "!config.graphics.space!" if /i NOT "!bdc.%_newX%.%_newY%!" == "!config.graphics.goal!" exit /b 0

		REM Check if the player is holding a block. 
		if /i "!game.hasBlock!" == "true" (
			REM check if there is space for the block above the desired location.
			if /i NOT "!bdc.%_newX%.%_newBlockY%!" == "!config.graphics.space!" exit /b 0
			REM Check if there is something overhead the block.
			if /i NOT "!bdc.%game.playerX%.%_newBlockY%!" == "!config.graphics.space!" exit /b 0
		) ELSE (
			REM Check if there is something overhead the player.
			if /i NOT "!bdc.%game.playerX%.%_newY%!" == "!config.graphics.space!" exit /b 0
		)
	)
	set game.playerX=!_newX!
	set game.playerY=!_newY!
	set /a game.moveCounter+=1
	call :updatePosition || exit /b !ERRORLEVEL!
	if /i NOT "!cheats.antigrav!" == "ON " call :playerGravity || exit /b !ERRORLEVEL!
exit /b 0

:: Pickup and place block
:pickupOrPlaceBlock
	set /a _checkX=!game.playerX! !game.direction! 1
	set /a _checkY=!game.playerY! - 1
	if !_checkX! GTR !bdc.mapWidth!  set _checkX=1
	if !_checkX! LSS 1                set _checkX=!bdc.mapWidth!
	if !_checkY! LSS 1                set _checkY=!bdc.mapHeight!

	if /i "!game.hasBlock!" == "true" (
		REM Place the block

		REM Check if there's anything above the desired dropping point.
		if /i NOT "!bdc.%_checkX%.%_checkY%!" == "!config.graphics.space!" exit /b

		set game.hasBlock=false
		call :updatePosition

		REM Check if there's anything at the desired dropping point.
		if /i "!bdc.%_checkX%.%game.playerY%!" == "!config.graphics.space!" (
			set bdc.!_checkX!.!game.playerY!=!config.graphics.block!
			call :setSymbolAtLocation "%_checkX%" "%game.playerY%" "%config.graphics.block%"
			call :locationGravity "!_checkX!" "!game.playerY!"
		) ELSE (
			REM If there is, place the block above it
			set bdc.!_checkX!.!_checkY!=!config.graphics.block!
			call :setSymbolAtLocation "%_checkX%" "%_checkY%" "%config.graphics.block%"
		)
	) ELSE (
		REM Pickup the block

		REM Check if there is a block to grab.
		if /i NOT "!bdc.%_checkX%.%game.playerY%!" == "!config.graphics.block!" exit /b

		REM Check if there's anything above the desired block.
		if /i NOT "!bdc.%_checkX%.%_checkY%!" == "!config.graphics.space!" exit /b

		REM Check if there's anything above the player.
		if /i NOT "!bdc.%game.playerX%.%_checkY%!" == "!config.graphics.space!" exit /b
		
		set bdc.!_checkX!.!game.playerY!=!config.graphics.space!
		call :setSymbolAtLocation "%_checkX%" "%game.playerY%" "%config.graphics.space%"
		set game.hasBlock=true
		call :updatePosition
	)
exit /b

:check_and_move_x sign
	REM Check if there is space for the player in the desired location.
	set /a _newX=!game.playerX! %~1 1
	if !_newX! GTR !bdc.mapWidth!  set _newX=1
	if !_newX! LSS 1                set _newX=!bdc.mapWidth!
	
	set /a _newY=!game.playerY! - 1
	if !_newY! LSS 1 set _checkY=!bdc.mapHeight!

	if /i NOT "!cheats.ghostdude!" == "ON " (
		REM Check if there's space adjacent to the player.
		if /i NOT "!bdc.%_newX%.%game.playerY%!" == "!config.graphics.space!" if /i NOT "!bdc.%_newX%.%game.playerY%!" == "!config.graphics.goal!" exit /b 0

		REM If the player is holding a block, check if there is space for the block above the desired location.
		if /i "!game.hasBlock!" == "true" (
			if /i NOT "!bdc.%_newX%.%_newY%!" == "!config.graphics.space!" exit /b 0
		)
	)
	set /a game.playerX=!_newX!
	set /a game.moveCounter+=1
	call :updatePosition || exit /b !ERRORLEVEL!
	if /i NOT "!cheats.antigrav!" == "ON " call :playerGravity || exit /b !ERRORLEVEL!
exit /b 0

:playerGravity
	set /a _newY=!game.playerY! + 1
	if not defined bdc.!game.playerX!.!_newY! set _newY=1

	set _flag=
	if /i "!bdc.%game.playerX%.%_newY%!" == "!config.graphics.space!" set _flag=1
	if /i "!bdc.%game.playerX%.%_newY%!" == "!config.graphics.goal!"  set _flag=1
	if defined _flag (
		call :display
		set /a game.playerY=!_newY!
		call :updatePosition || exit /b !ERRORLEVEL!
		goto playerGravity
	)
exit /b 0

:locationGravity
	REM Save off the symbol and get coordinates for space below it.
	set _sym=!bdc.%~1.%~2!
	set /a _newY=%~2 + 1
	if not defined bdc.%~1.!_newY! set _newY=1

	REM Check if there's anything below the object.
	if /i "!bdc.%~1.%_newY%!" == "!config.graphics.space!" (
		REM If there is, drop it one space, display, and recursively check again.
		call :display
		set bdc.%~1.%~2=!config.graphics.space!
		call :setSymbolAtLocation "%~1" "%~2" "!config.graphics.space!"
		set bdc.%~1.!_newY!=!_sym!
		call :setSymbolAtLocation "%~1" "!_newY!" "!_sym!"
		call :locationGravity "%~1" "!_newY!"
	)
exit /b

:updatePosition
	REM Restore what was originally in the player's and block's spaces
	set bdc.!game.prevPlayerX!.!game.prevPlayerY!=!game.prevPlayerLocSym!
	set bdc.!game.prevPlayerX!.!game.prevBlockY!=!game.prevBlockLocSym!
	call :setSymbolAtLocation "!game.prevPlayerX!" "!game.prevPlayerY!" "!game.prevPlayerLocSym!"
	call :setSymbolAtLocation "!game.prevPlayerX!" "!game.prevBlockY!"  "!game.prevBlockLocSym!"

	REM Save off the player's current location as the previous location
	set game.prevPlayerX=!game.playerX!
	set game.prevPlayerY=!game.playerY!
	set /a game.prevBlockY=!game.playerY! - 1
	if !game.prevBlockY! LSS 1 set game.prevBlockY=!bdc.mapHeight!

	REM Save off what's in the player's and block's spaces
	set game.prevPlayerLocSym=!bdc.%game.playerX%.%game.playerY%!
	set game.prevBlockLocSym=!bdc.%game.playerX%.%game.prevBlockY%!

	REM Move the player
	set bdc.!game.playerX!.!game.playerY!=!config.graphics.head!
	call :setSymbolAtLocation "!game.playerX!" "!game.playerY!" "!config.graphics.head!"

	REM Move the block if we have one
	if /i "!game.hasBlock!" == "true" (
		set bdc.!game.playerX!.!game.prevBlockY!=!config.graphics.block!
		call :setSymbolAtLocation "!game.playerX!" "!game.prevBlockY!" "!config.graphics.block!"
	)

	REM Check if the player won
	if /i "!game.prevPlayerLocSym!" == "!config.graphics.goal!" (
		call :display
		exit /b 4
	)

	set /a _deadY=!game.playerY! + 1
	if !_deadY! GTR !bdc.mapHeight! set _deadY=1
	if /i NOT "!cheats.immortality!" == "ON " (
		REM Game over if we've stepped on a deadly object
		if /i "!bdc.%game.playerX%.%_deadY%!" == "!config.graphics.death!" (
			REM Animate death. This could definitely be refactored
			set /a _above=!game.playerY! - 1
			call :setSymbolAtLocation "!game.playerX!" "!_above!" "" & call :display & ping 192.0.2.2 -n 1 -w 1000 >nul
			call :setSymbolAtLocation "!game.playerX!" "!_above!" "!config.graphics.space!"
			call :setSymbolAtLocation "!game.playerX!" "!game.playerY!" "0" & call :display & ping 192.0.2.2 -n 1 -w 500 >nul
			call :setSymbolAtLocation "!game.playerX!" "!game.playerY!" "O" & call :display & ping 192.0.2.2 -n 1 -w 500 >nul
			call :setSymbolAtLocation "!game.playerX!" "!game.playerY!" "*" & call :display & ping 192.0.2.2 -n 1 -w 500 >nul
			call :setSymbolAtLocation "!game.playerX!" "!game.playerY!" "." & call :display & ping 192.0.2.2 -n 1 -w 500 >nul
			call :setSymbolAtLocation "!game.playerX!" "!game.playerY!" " " & call :display & ping 192.0.2.2 -n 1 -w 500 >nul
			exit /b 3
		)
	)
exit /b 0

:setSymbolAtLocation x y symbol
	REM echo x: %~1, y: %~2, map width: %bdc.mapWidth%, height: %bdc.mapHeight%
	REM pause
	if "%~1" == "" exit /b
	if "%~2" == "" exit /b
	set /a _offset=%~1 - 1
	set /a _offset+=((%bdc.mapWidth% + 1) * (%~2 - 1))
	REM echo Offset number for %~1.%~2: !_offset!
	REM echo found: [!game.disp:~%_offset%,1!]
	REM pause
	set /a _r=%_offset%+1
	set game.disp_l=!game.disp:~0,%_offset%!
	set game.disp_r=!game.disp:~%_r%!
	set game.disp=!game.disp_l!%~3!game.disp_r!
exit /b

:inGameMenu
	cls
	echo.
	echo  Too hard? ;)
	echo.
	echo  1. Continue
	echo  2. Restart
	echo  3. Quit
	echo  4. Configure
	echo  5. Debug Mode - %debugmode%
	echo.
	echo  [b] Back
	echo.
	set /p _input=Choose a number: 
	if /i "%_input%" == "b" exit /b 0
	if "%_input%" == "1" exit /b 0
	if "%_input%" == "2" exit /b 2
	if "%_input%" == "3" exit /b 1
	if "%_input%" == "4" call BatchDudeConfig.bat
	if "%_input%" == "5" (
		if /i "%debugmode%" == "Off" (set debugmode=On) ELSE set debugmode=Off
	)
goto inGameMenu

::: GAME END :::

:about
	cls
	REM Dedication
	echo.
	echo  "Whatever you do, work at it with all your heart, as working for the Lord, not
	echo  for human masters." (Colossians 3:23 NIV)
	echo.
	echo  I thank my God for the opportunity and ability to do this.
	echo.
	echo  Special thanks to my wife who encouraged me to finish what I started long ago.
	echo.
	ping 192.0.2.2 -n 1 -w 2000 >nul
	pause
	:about_sub1
	cls
	echo.
	echo.
	echo         栢栢栢栢�                                               
	echo         昉께께께�                                             
	echo         昉께께께�            -+= BatchDude =+-                    
	echo         昉께께께�                                                 
	echo         昉께께께�                                                
	echo         栢栢栢栢�                                                
	echo                                                               
	echo                          Release  v1.0                    
	echo                               By Kolto101                  栢栢栢栢�
	echo                          Tester/Editor - Yuvira            昉께께께�
	echo                            Copyright (c) 2019              昉께께께�
	echo                                                           昉께께께�
	echo                                                             昉께께께�
	echo                                                          栢栢栢栢�
	echo.
	echo.
	echo.
	echo  Release Notes:
	echo.
	echo  A tribute to Brandon Sterner's "Block Dude" for the TI-83+ Calculator.
	echo.
	echo  ** PLEASE report ANY bugs or crashes that may occur to: kolto101@gmail.com **
	echo.             Visit kolto101's YouTube for updates and more
	echo.
	echo.
	echo  [b] Back
	echo.
	set /p _input=Choose a letter: 
	if /i "%_input%" == "b" goto menu
goto about_sub1

:howtoplay
	REM mode 81,26

	cls
	echo.
	echo    BatchDude
	echo    Tutorial
	echo.
	echo 1. Objective
	echo 2. Custom Maps
	echo 3. Cheats
	echo 4. Other Notes
	echo.
	echo b/Back
	echo.
	set /p _input=Select a section number 
	if /i "!_input!" == "b" goto menu
	if "!_input!" == "1" call :objective
	if "!_input!" == "2" call :custommapnotes
	if "!_input!" == "3" call :cheatnotes
	if "!_input!" == "4" call :othernotes
goto howtoplay

:objective
	cls
	title BatchDude Tutorial - The Objective
	echo.
	echo 1 - The Objective
	echo ------------------
	echo.
	echo   Main Objective 
	echo.
	echo  Reach the goal of the level using the various blocks placed througout each map.
	echo  Blocks may be climbed, stacked, and carried by BatchDude.
	echo.
	echo   Losing 
	echo.
	echo  The player loses if: 1. BatchDude steps over deadly objects. 2. BatchDude is
	echo  unable to reach the goal. The player may get caught and be unable to move, or
	echo  simply did not place the blocks in the correct spot. In this case, the player
	echo  must restart the level.
	echo.
	echo                                  Uh-oh!
	echo                             � � /               
	echo                             ��                 
	echo                             栢�               栢栢
	echo.
	echo.
	echo                             PRESS ANY KEY TO CONTINUE
	pause>nul
exit /b

:custommapnotes
	cls
	title BatchDude Tutorial - Custom Maps
	echo.
	echo 2 - Custom Maps
	echo ----------------
	echo.
	echo   Custom Maps 
	echo.
	echo  Custom maps may be created using the Editor. Maps may be in all different
	echo  sizes, with varying borders and terrain. 
	echo.
	echo  All maps MUST be located in the MAPS folder in order to load them.
	echo.
	echo.
	echo                             PRESS ANY KEY TO CONTINUE
	pause>nul
exit /b

:cheatnotes
	mode 81,36
	cls
	title BatchDude Tutorial - Cheats
	echo.
	echo 4 - Cheats
	echo -----------
	echo.
	echo   Sharp Turns 
	echo.
	echo  Normally, BatchDude cannnot make a 360� turn while staying in one position. If
	echo  this cheat is turned on, then BatchDude will change direction without moving
	echo  a space. This means that the player must press the same button twice to begin
	echo  moving in another direction.
	echo.
	echo   Ghost Dude 
	echo.
	echo  If the Ghost Dude cheat is on, then BatchDude will have the ability to pass
	echo  through blocks HORIZONTALLY. BatchDude will not fall through blocks. BatchDude
	echo  will also still have the ability to pick up and place blocks.
	echo.
	echo   Anti-grav Boots 
	echo.
	echo  Anti-grav boots will keep BatchDude from falling when there is not a block
	echo  underneath. This WILL NOT protect against deadly objects.
	echo  NOTE: To simulate gravity, press G or E while in game.
	echo.
	echo   Immortality 
	echo.
	echo  Immortality will keep BatchDude from dying upon contact with deadly objects. 
	echo.
	echo   Show Classic Levels 
	echo.
	echo  Load a classic level level directly without beating previous levels.
	echo.
	echo                             PRESS ANY KEY TO CONTINUE
	pause>nul
exit /b


:othernotes
	cls
	title BatchDude Tutorial - Other Notes
	echo.
	echo 5 - Other Notes
	echo ----------------
	echo.
	echo -BatchDude requires the choice command.
	echo.
	echo -Created using code page 437
	echo.
	echo -Report any crashes, bugs, missing features, or special requests to:
	echo  kolto101@gmail.com
	echo.
	echo.
	echo                             PRESS ANY KEY TO CONTINUE
	pause>nul
exit /b