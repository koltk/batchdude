@echo off


REM By Kolto101

if "%~1" == "start" goto main
:start_loop
	cmd /V:ON /Q /c "BatchDudeEditor.bat start"
	echo.
	if "%ERRORLEVEL%" == "0" (
		echo Script finished successfully.
	) ELSE (
		echo Script crashed.
		echo.
		echo ERRORLEVEL: %ERRORLEVEL%
		echo.
		echo If the game is unmodified, please report the incident to:
		echo kolto101@gmail.com
	)
	:run_again_loop
		echo.
		set /p _input=Run again? [y/n]: 
		if /i "%_input%" == "y" goto start_loop
		if /i "%_input%" == "n" EXIT
goto run_again_loop

:main

call BatchDudeConfig.bat "init"
setlocal EnableDelayedExpansion

REM Conduct a read/write. To make a map, we will need read/write priviledges
echo true>_writeTestFile.map
if /i NOT exist _writeTestFile.map (
	echo.
	echo ERROR: Failed to write to test file. Created maps will not be able to save.
	echo        Try running as admin?
	echo.
	pause
) ELSE (
	set /p _testVar=<_writeTestFile.map

	if /i NOT "!_testVar!" == "true" (
		echo.
		echo ERROR: Failed to read from test file. Maps will not be able to load.
		echo.
		pause
	)
	set _testVar=
	del /f _writeTestFile.map
)

:menu
	mode 80,40
	title BatchDude Map Editor
	cls
	echo.
	echo BatchDude Map Editor
	echo.
	echo  1. Create New Map
	echo  2. Load Map
	echo  3. Configure
	echo  4. Exit
	echo.
	set /p _input=Choose a number: 
	if "%_input%" == "1" call :create_new_map
	if "%_input%" == "2" call :load_map
	if "%_input%" == "3" call BatchDudeConfig.bat
	if "%_input%" == "4" EXIT /b 0
goto menu

:clearAllVars
	for %%a in (bdc. edit. _) do (
		(set %%a> nul 2>&1) && (
			for /f "tokens=1 delims==" %%b in ('set %%a') do set %%b=
	))
exit /b

:create_new_map
	call :clearAllVars
	echo.
	echo Enter the name, width, and height of the map.
	echo.
	echo b/Back
	echo.
	set /p edit.filename=Name: 
	if /i "!edit.filename!" == "b" exit /b

	call :set_dimensions || exit /b

	call :init
exit /b

:set_dimensions
	set _oldWidth=0
	set _oldHeight=0

	if defined bdc.mapWidth  set _oldWidth=!bdc.mapWidth!
	if defined bdc.mapHeight set _oldHeight=!bdc.mapHeight!

	set /p bdc.mapWidth=Width: 
	set /p bdc.mapHeight=Height: 

	REM Clear variables not within bounds
	for /l %%r in (1,1,!_oldHeight!) do (
		for /l %%c in (1,1,!_oldWidth!) do (
			if %%r GTR !bdc.mapHeight! set bdc.%%c.%%r=
			if %%c GTR !bdc.mapWidth!  set bdc.%%c.%%r=
	))
exit /b 0

:load_map
	call :clearAllVars
	set _maxFiles=0
	set NUM_SHOW_FILES=10

	if /i "%cheats.showClassicLevels%" == "ON " (
		for /l %%c in (1,1,11) do (
			set /a _maxFiles+=1
			set _fname.!_maxFiles!=Classic\level%%c
 		)
 	)

	for /f "tokens=*" %%f in ('dir /b /a-d /on "MAPS\"') do (
		set /a _maxFiles+=1
		set _fname.!_maxFiles!=%%~nf
	)
	set _mStart=1
	set _mEnd=!NUM_SHOW_FILES!

	:load_map_sub1
		cls
		echo.
		if !_mStart! LSS 1 (
			set _mStart=1
			set _mEnd=!NUM_SHOW_FILES!
		)
		if !_mStart! GTR !_maxFiles! (
			set /a _mEnd-=!NUM_SHOW_FILES!
			set /a _mStart-=!NUM_SHOW_FILES!
		)
		for /l %%m in (!_mStart!,1,!_mEnd!) do (
			if defined _fname.%%m (echo  %%m. !_fname.%%m!) ELSE echo.
		)
		echo.
		if !_maxFiles! GTR !NUM_SHOW_FILES! (
			echo  [b] Back   [w] Scroll up   [s] Scroll down
		) ELSE echo  [b] Back
		echo.
		set /p _input=Enter: 
		if /i "!_input!" == "b" exit /b
		if /i "!_input!" == "w" (
			set /a _mStart-=!NUM_SHOW_FILES!
			set /a _mEnd-=!NUM_SHOW_FILES!
			goto :load_map_sub1

		) ELSE 	if /i "!_input!" == "s" (
			set /a _mStart+=!NUM_SHOW_FILES!
			set /a _mEnd+=!NUM_SHOW_FILES!
			goto :load_map_sub1

		) ELSE if !_input! GEQ 1 if !_input! LEQ !_maxFiles! set _input=!_fname.%_input%!

	if /i exist "MAPS\!_input!"  set _input=!_input:.map=!
	if /i NOT exist "MAPS\!_input!.map" goto :load_map_sub1
	
	set edit.filename=!_input!

	call :init "!_input!"
exit /b

:init
	if NOT "%~1" == "" (
		for /f "tokens=1* delims==" %%a in (MAPS\%~1.map) do (
			set %%a=%%b
			if defined config.graphics.%%b set %%a=!config.graphics.%%b!
		)
	)

	REM Fill undefined variables with spaces
	for /l %%r in (1,1,!bdc.mapHeight!) do (
		for /l %%c in (1,1,!bdc.mapWidth!) do (
			if not defined bdc.%%c.%%r set bdc.%%c.%%r=!config.graphics.space!
	))

	REM Build the display screen and store it as a variable.
	set edit.disp=
	for /l %%h in (1,1,%bdc.mapHeight%) do (
		set edit.disp=!edit.disp!�
		for /l %%w in (1,1,%bdc.mapWidth%) do set edit.disp=!edit.disp!!bdc.%%w.%%h!
		set edit.disp=!edit.disp!�!LF!
	)

	REM Create a hard border around the map
	set edit.borderBottom=
	set edit.borderTop=
	for /l %%b in (1,1,!bdc.mapWidth!) do set edit.borderTop=!edit.borderTop!�
	set edit.borderBottom=!edit.borderTop!
	set edit.borderTop=�!edit.borderTop!�
	set edit.borderBottom=�!edit.borderBottom!�

	set edit.disp=!edit.borderTop!!LF!!edit.disp!!edit.borderBottom!

	set edit.x=1
	set edit.y=1
	set edit.tool=Single
	set edit.out=space
	call :setSymbolAtLocation "!edit.x!" "!edit.y!" "X"

	call :edit_loop
exit /b

:setSymbolAtLocation x y symbol
	if "%~1" == "" exit /b
	if "%~2" == "" exit /b
	set /a _offset=%~1 + %bdc.mapWidth% + 3
	set /a _offset+=((%bdc.mapWidth% + 3) * (%~2 - 1))

	set /a _r=%_offset%+1
	set _disp_l=!edit.disp:~0,%_offset%!
	set _disp_r=!edit.disp:~%_r%!
	if defined config.graphics.%~3 (
		set edit.disp=!_disp_l!!config.graphics.%~3!!_disp_r!
	) ELSE set edit.disp=!_disp_l!%~3!_disp_r!
exit /b

:edit_loop
	cls
	call :setSymbolAtLocation "!bdc.playerX!" "!bdc.playerY!" "head"
	echo.
	echo  !edit.tool!: [!edit.out!]   X/Y: !edit.x!/!edit.y! -- [!bdc.%edit.x%.%edit.y%!]
	echo.
	echo !edit.disp!
	echo.
	echo [1] Border   [2] Block   [3] Space
	echo [4] Death    [5] Goal    [6] Start
	echo.
	echo [u] Use Tool   [c] Cycle Tools   [x] Copy   [p] Print to file
	echo [m] Metadata   [z] Save          [q] Quit
	choice /c:wasdm123456uczqvxp /n
	     if "!ERRORLEVEL!" == "1"  (call :move "y" "-")^
	ELSE if "!ERRORLEVEL!" == "2"  (call :move "x" "-")^
	ELSE if "!ERRORLEVEL!" == "3"  (call :move "y" "+")^
	ELSE if "!ERRORLEVEL!" == "4"  (call :move "x" "+")^
	ELSE if "!ERRORLEVEL!" == "5"  (call :edit_metadata)^
	ELSE if "!ERRORLEVEL!" == "6"  (set edit.out=border)^
	ELSE if "!ERRORLEVEL!" == "7"  (set edit.out=block)^
	ELSE if "!ERRORLEVEL!" == "8"  (set edit.out=space)^
	ELSE if "!ERRORLEVEL!" == "9"  (set edit.out=death)^
	ELSE if "!ERRORLEVEL!" == "10" (set edit.out=goal)^
	ELSE if "!ERRORLEVEL!" == "11" (set edit.out=start)^
	ELSE if "!ERRORLEVEL!" == "12" (call :use_tool)^
	ELSE if "!ERRORLEVEL!" == "13" (call :cycle_tools)^
	ELSE IF "!ERRORLEVEL!" == "14" (call :save_map)^
	ELSE IF "!ERRORLEVEL!" == "15" (call :quit_edit && exit /b)^
	ELSE IF "!ERRORLEVEL!" == "16" (set edit.out=!bdc.%edit.x%.%edit.y%!)^
	ELSE IF "!ERRORLEVEL!" == "17" (call :print_screen)
goto :edit_loop

:print_screen
	echo !edit.disp! > batchdude_level_print.txt
	echo !LF!Screen printed to batchdude_level_print.txt!LF! & pause
exit /b

:move
	REM Check if we should leave a configured "graphic" or hard-coded value.
	if defined config.graphics.!bdc.%edit.x%.%edit.y%! (
		for %%v in (!bdc.%edit.x%.%edit.y%!) do (
			call :setSymbolAtLocation "!edit.x!" "!edit.y!" "!config.graphics.%%v!"
	)) ELSE call :setSymbolAtLocation "!edit.x!" "!edit.y!" "!bdc.%edit.x%.%edit.y%!"

	set /a edit.%~1%~2=1

	if !edit.x! GTR !bdc.mapWidth!  set edit.x=1
	if !edit.y! GTR !bdc.mapHeight! set edit.y=1
	if !edit.x! LSS 1               set edit.x=!bdc.mapWidth!
	if !edit.y! LSS 1               set edit.y=!bdc.mapHeight!

	call :setSymbolAtLocation "!edit.x!" "!edit.y!" "X"
exit /b

:edit_metadata
	cls
	echo.
	echo Edit Menu
	echo.
	if defined bdc.author (
		echo 1. Author        !bdc.author!
	) ELSE echo 1. Author        [unset]
	echo.
	if defined bdc.mapname (
	echo 2. Level name    !bdc.mapname!
	) ELSE echo 2. Level name    [unset]
	echo.
	echo 3. Save-as       !edit.filename!
	echo.
	echo 4. Screen size   w: !bdc.mapWidth! y: !bdc.mapWidth!
	echo.
	if defined bdc.solution (
		echo 5. Solution      [set]
	) ELSE echo 5. Solution      [unset]

	echo.
	echo [b] Back
	echo.
	set /p _input=Choose a number: 
	if /i "!_input!" == "b" exit /b
	if "!_input!" == "1" (
		echo.
		echo Save new author name.
		echo Current: !bdc.author!
		echo.
		set /p bdc.author=New author: 
	) ELSE if "!_input!" == "2" (
		echo.
		echo Save new map name.
		echo Current: !bdc.mapname!
		echo.
		set /p bdc.mapname=New map name: 
	) ELSE if "!_input!" == "3" (
		echo.
		echo Save as a new file.
		echo Current: !edit.filename!
		echo.
		set /p edit.filename=New filename: 
	) ELSE if "!_input!" == "4" (
		call :set_dimensions
	) ELSE if "!_input!" == "5" (
		if defined bdc.solution (
			echo.
			echo 1. Play solution
			echo 2. Unset solution
			echo 3. Change solution
			echo.
			set /p _input=Choose a number: 
			if "!_input!" == "1" (
				REM Play solution
				call :save_tmp_map
				call NewBatchDude.bat "play_solution" "__tmp.map"
				call :restore_from_tmp_map
			) ELSE if "!_input!" == "2" (
				REM Unset solution
				set bdc.solution=
			)

			if NOT "!_input!" == "3" goto :edit_metadata
			REM ::: Fall through for option 3 :::
		)
		REM Set or change solution
		REM Make sure there's a start before trying.
		if defined bdc.playerX (
			call :save_tmp_map
			set edit.solutionFound=
			call NewBatchDude.bat "record" "__tmp.map" || if "!ERRORLEVEL!" == "4" set edit.solutionFound=1
			call :restore_from_tmp_map

			REM Set solution [if found] after map restore so we don't erase it
 			if defined edit.solutionFound set bdc.solution=!$_playback!
		) ELSE echo !LF!You must first designate a start.!LF! & pause
	)
goto :edit_metadata

:save_tmp_map
	REM Save off edit map to tmp file for main game to load
	for /f "tokens=1* delims==" %%a in ('set bdc.') do (
		set tmp_%%a=%%b
		if /i "%%b" == "space" set %%a=
	)
	set bdc>MAPS\__tmp.map
exit /b

:restore_from_tmp_map
	REM Restore edit map
	for /f "tokens=1* delims=_" %%a in ('set tmp_bdc.') do set %%b

	REM Remove the __tmp.map file
	if /i exist MAPS\__tmp.map del /f MAPS\__tmp.map
exit /b

:quit_edit
	::Q-quit -- Clears screen for the next map loaded in.
	echo.
	set /p _input=Save? [y/n/c]: 
	if /i "!_input!" == "c" exit /b 1
	if /i not "!_input!" == "n" if /i not "!_input!" == "y" goto quit
	if /i "!_input!" == "y" call :save_map
exit /b 0


:save_map
	::Z-save
	if not exist MAPS mkdir MAPS
	echo.
	echo Saving... Please wait...

	REM Don't hardcode the graphics, just say what's there
	for /f "tokens=1* delims==" %%a in ('set bdc.') do (
		if /i "!config.graphics.border!" == "%%b" set %%a=border
		if /i "!config.graphics.block!"  == "%%b" set %%a=block
		if /i "!config.graphics.goal!"   == "%%b" set %%a=goal
		if /i "!config.graphics.death!"  == "%%b" set %%a=death
		REM Leave spaces undefined. This means saving off the map and restoring it after saving
		set tmp_%%a=%%b
		if /i "!config.graphics.space!"  == "%%b" set %%a=
		REM if /i "%%b" == "space" set %%a=
	)

	set bdc>MAPS\!edit.filename!.map

	REM Restore the map since we undefined spaces
	for /f "tokens=1* delims=_" %%a in ('set tmp_bdc.') do set %%b
exit /b

:cycle_tools
	if /i "!edit.tool!" == "Single" (
		set edit.tool=Rectangle
	) ELSE if /i "!edit.tool!" == "Rectangle" (
		set edit.tool=Single
		set recx1=
		set srecx1=
	)
exit /b

:use_tool
	::P-Use Tool

	if /i "!edit.out!" == "start" (
		call :setSymbolAtLocation "!bdc.playerX!" "!bdc.playerY!" "!bdc.%bdc.playerX%.%bdc.playerY%!"
		set bdc.playerX=!edit.x!
		set bdc.playerY=!edit.y!
		exit /b
	)

	if /i "!edit.tool!" == "Single" (
		set bdc.!edit.x!.!edit.y!=!edit.out!
		call :setSymbolAtLocation "!edit.x!" "!edit.y!" "!edit.out!"
	)
	if /i "!edit.tool!" == "Rectangle" call :rectangle
exit /b

:rectangle
	if not defined recx1 (
		set recx1=!edit.x!
		set recy1=!edit.y!
		set bdc.!edit.x!.!edit.y!=!edit.out!
		call :setSymbolAtLocation "!edit.x!" "!edit.y!" "!edit.out!"
		exit /b
	)
	set recx2=!edit.x!
	set recy2=!edit.y!

	if !recx2! LSS !recx1! (set signr=-1) ELSE set signr=1
	if !recy2! LSS !recy1! (set signh=-1) ELSE set signh=1

	for /l %%h in (!recy1!,!signh!,!recy2!) do (
		for /l %%r in (!recx1!,!signr!,!recx2!) do (
			set bdc.%%r.%%h=!edit.out!
			call :setSymbolAtLocation "%%r" "%%h" "!edit.out!"
	))
	set recx1=
exit /b