:: Accessor for external files
@if NOT "%~1" == "" (
	if /i "%~1" == "init" (
		call :init
	) ELSE if /i "%~1" == "cheats_off" (
		call :defaults_cheats
	)
) ELSE (
	call :config
)
exit /b !ERRORLEVEL!

:init
	REM Init only once
	if defined config_init exit /b
	set config_init=1

	REM Set macros before delayed expansion
	REM define a Line Feed [newline] string [normally only used as !LF!]
	set LF=^


	::Above 2 blank lines are required - do not remove

	::define a newline with line continuation
	set ^"\n=^^^%LF%%LF%^%LF%%LF%^^"
	::"

	call :defaults_all
	if /i exist "batchdude_config.ini" for /f "tokens=1* delims==" %%a in (batchdude_config.ini) do set %%a=%%b
	color %config.bf%
exit /b

:config_save
	set config.>batchdude_config.ini
exit/b

:defaults_all
	call :defaults_graphics1
	call :defaults_cheats
	call :defaults_input1
	set config.bf=07
	set debugmode=Off
exit /b

:defaults_input1
	set config.input=swadef
exit /b

:defaults_input2
	set config.input=kijluh
exit /b

:defaults_graphics
:defaults_graphics1
	set config.graphics.border=�
	set config.graphics.block=@
	set config.graphics.space= 
	set config.graphics.head=
	set config.graphics.death=
	set config.graphics.goal=

exit /b

:defaults_graphics2
	set config.graphics.border=�
	set config.graphics.block=�
	set config.graphics.space= 
	set config.graphics.head=
	set config.graphics.death=
	set config.graphics.goal=�

:defaults_cheats
	set cheats.sharpturn=Off
	set cheats.ghostdude=Off
	set cheats.antigrav=Off
	set cheats.immortality=Off
	set cheats.showClassicLevels=Off
exit /b


:config
	cls
	echo.
	echo  ��������������������ͻ
	echo  �     Configure      �
	echo  ��������������������͹
	echo  �                    �
	echo  � 1. Input           �
	echo  � 2. Customize       �
	echo  � 3. Themes          �
	echo  � 4. Cheats          �
	echo  �                    �
	echo  ��������������������ͼ
	echo.
	echo  [b] Back
	echo.
	set /p _input=Choose a number: 
	if /i "%_input%" == "b" exit /b
		 if "%_input%" == "1" (call :config_input)^
	ELSE if "%_input%" == "2" (call :config_graphics)^
	ELSE if "%_input%" == "3" (call :config_themes)^
	ELSE if "%_input%" == "4" (call :config_cheats)
goto config

:config_cheats
	cls
	echo.
	echo  ���������������������������������������ͻ
	echo  �                 Cheats                �
	echo  ���������������������������������������͹
	echo  �                                       �
	echo  �  1. Sharp Turn                   %cheats.sharpturn%  �
	echo  �  2. Ghost Dude                   %cheats.ghostdude%  �
	echo  �  3. Anti-Grav Boots              %cheats.antigrav%  �
	echo  �  4. Immortality                  %cheats.immortality%  �
	echo  �  5. Show Classic Levels          %cheats.showClassicLevels%  �
	echo  �                                       �
	echo  ���������������������������������������ͼ
	echo.
	echo  [b] Back  [n] All On  [f] All Off  [h] Help
	echo.
	set /p _input=Choose a number: 
	if /i "%_input%" == "h" call :cheatnotes
	if /i "%_input%" == "b" exit /b
	if /i "%_input%" == "n" for %%c in (sharpturn ghostdude antigrav immortality showClassicLevels) do set cheats.%%c=ON 
	if /i "%_input%" == "f" for %%c in (sharpturn ghostdude antigrav immortality showClassicLevels) do set cheats.%%c=OFF

	if "%_input%" == "1" call :toggleOnOff "cheats.sharpturn"
	if "%_input%" == "2" call :toggleOnOff "cheats.ghostdude"
	if "%_input%" == "3" call :toggleOnOff "cheats.antigrav"
	if "%_input%" == "4" call :toggleOnOff "cheats.immortality"
	if "%_input%" == "5" call :toggleOnOff "cheats.showClassicLevels"
goto config_cheats


:toggleOnOff varname
	if /i "!%~1!" == "OFF" (set %~1=ON ) ELSE set %~1=OFF
exit /b

:config_themes
	set flag=
	cls
	echo.
	echo                           0 = Black      8 = Gray
	echo                           1 = Blue       9 = Light Blue
	echo                           2 = Green      A = Light Green
	echo                           3 = Aqua       B = Light Aqua
	echo                           4 = Red        C = Light Red
	echo                           5 = Purple     D = Light Purple
	echo                           6 = Yellow     E = Light Yellow
	echo                           7 = White      F = Bright White
	echo.
	echo                                   Default: 07
	echo.
	echo               Set the background color and the foreground color.
	echo               The first letter/number is the background color,
	echo               the second is the foreground. Do not use spaces.
	echo.
	echo                                [q] Back  [i] Invert
	echo.
	set /p _input=Set Background/Foreground: 
	if /i "!_input!" == "q" exit /b
	if /i "!_input!" == "i" (
		set config.bf=!config.bf:~1,1!!config.bf:~0,1!
		color !config.bf!
		call :config_save
	)
	set _input=!_input:~0,2!
	if "!_input:~1,1!" == "" set _input=0!_input!
	set flag=0
	for %%b in (0 1 2 3 4 5 6 7 8 9 A B C D E F) do (
		if /i "!_input:~0,1!" == "%%b" set /a flag+=1
		if /i "!_input:~1,1!" == "%%b" set /a flag+=1
	)
	if "!flag!" == "2" (
		color !_input! || goto config_themes
		set config.bf=!_input!
		call :config_save
	)
goto config_themes

:config_input
	cls
	echo.
	echo  �������������������ͻ
	echo  �       Input       �
	echo  �������������������͹
	echo  �                   �
	echo  � 1. Block: %config.input:~0,1%       �
	echo  �                   �
	echo  � 2. Up: %config.input:~1,1%          �
	echo  �                   �
	echo  � 3. Left: %config.input:~2,1%        �
	echo  �                   �
	echo  � 4. Right: %config.input:~3,1%       �
	echo  �                   �
	echo  � 5. Menu: %config.input:~4,1%        �
	echo  �                   �
	echo  � 6. Gravity: %config.input:~5,1%     �
	echo  �                   �
	echo  �������������������ͼ
	echo.
	echo  [b] Back   [d1] WASD Preset   [d2] IJKL Preset
	echo.
	set /p _input=Choose a number: 
	if /i "%_input%" == "b" exit /b
	if /i "%_input%" == "d1" call :defaults_input1 && call :config_save
	if /i "%_input%" == "d2" call :defaults_input2 && call :config_save
	if %_input% GEQ 1 if %_input% LEQ 6 (
		set /a _inputIndex=!_input! - 1
		for %%i in (!_inputIndex!) do (
			echo Current: !config.input:~%%i,1!
			echo.
			set /p _input=Enter a letter or number [case insensitive]: 
			REM Check if only one character was entered
			if NOT "!_input:~1,1!" == "" (
				echo !LF!Only one character is allowed.!LF! && pause
			)  ELSE (
				set _validInput=1
				REM Check if we have a valid character
				call :contains_substring "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"  "!_input!" || (
					set _validInput=
					echo !LF!Invalid input.!LF! && pause
				)
				REM Check if the character is already being used
				if defined _validInput call :contains_substring "!config.input!" "!_input!" && (
					set _validInput=
					echo !LF!This character is already used.!LF! &&	pause
				)
				if defined _validInput (
					set /a _ri=%%i + 1
					set _l=!config.input:~0,%%i!
					for %%r in (!_ri!) do set _r=!config.input:~%%r!
					set config.input=!_l!!_input!!_r!
					call :config_save
				)
			)
		)
	)
goto config_input

:: Exit on 0 means substring found in string. On 1 means not found.
:contains_substring string substring
	set _str=%~1
	if /i NOT "!_str:%~2=!" == "!_str!" exit /b 0
exit /b 1

:config_graphics
	mode 80,30
	cls
	echo.
	echo  ��������������������ͻ
	echo  �     Customize      �
	echo  ��������������������͹
	echo  �                    �
	echo  � 1. Head: %config.graphics.head%         �
	echo  �                    �
	echo  � 2. Block: %config.graphics.block%        �
	echo  �                    �
	echo  � 3. Space: %config.graphics.space%        �
	echo  �                    �
	echo  � 4. Border: %config.graphics.border%       �
	echo  �                    �
	echo  � 5. Death: %config.graphics.death%        �
	echo  �                    �
	echo  � 6. Goal: %config.graphics.goal%         �
	echo  �                    �
	echo  ��������������������ͼ
	echo.
	echo  [b] Back   [d1] Preset 1   [d2] Preset 2
	echo.
	set /p _input=Choose a number to change the current graphic: 
	if /i "%_input%" == "b" exit /b
	if /i "%_input%" == "d1" call :defaults_graphics1 && call :config_save
	if /i "%_input%" == "d2" call :defaults_graphics2 && call :config_save
	if "%_input%" == "1" set _toChange=head
	if "%_input%" == "2" set _toChange=block
	if "%_input%" == "3" set _toChange=space
	if "%_input%" == "4" set _toChange=border
	if "%_input%" == "5" set _toChange=death
	if "%_input%" == "6" set _toChange=goal
	if %_input% GEQ 1 if %_input% LEQ 6 (
		call :change_symbol "_toChange"
		if defined $_return (
			set _valid=1
			for %%s in (head block space border death goal) do if not "!_toChange!" == "%%s" (
				if "!config.graphics.%%s!" == "!$_return!" (
					echo !LF!This character is already used.!LF! & pause
					set _valid=
			))
			if defined _valid (
				set config.graphics.!_toChange!=!$_return!
				call :config_save
			)
		)
	)
goto config_graphics

:change_symbol
	cls
	echo.
	for %%v in (!%~1!) do echo Currently editting: !%~1! [!config.graphics.%%v!]
	echo.
	echo Enter a character below, or choose an extended character.
	echo.
	echo  [-b] Back   [-s] Open extended characters
	echo.
	set /p _input=Chracter: 
	set _return= 
	if /i "%_input%" == "-b" exit /b
	if /i "%_input%" == "-s" (
		call :choose_extended_character
	) ELSE if not "%_input:~1%"=="" (
		echo !LF!You may only use 1 character.!LF! & pause
	) ELSE set $_return=%_input%
	if defined $_return exit /b
goto change_symbol


:choose_extended_character
	mode 80,50
	echo.
	echo      1:      2:      3:      4:      5:      6:      11:     12: 
	echo.
	echo      14:     15:     16:     17:     18:     19:     20:     21: 
	echo.
	echo      22:     23:     24:     25:     27:     28:     29:     30:    
	echo.
	echo      31: 
	echo.
	echo                 Characters 32-126 can be accessed via keyboard.
	echo.
	echo.
	echo      127:    128: �   129: �   130: �   131: �   132: �   133: �   134: �
	echo.
	echo      135: �   136: �   137: �   138: �   139: �   140: �   141: �   142: �
	echo.
	echo      143: �   144: �   145: �   146: �   147: �   148: �   149: �   150: �
	echo.
	echo      151: �   152: �   153: �   154: �   155: �   156: �   157: �   158: �
	echo.
	echo      159: �   160: �   161: �   162: �   163: �   164: �   165: �   166: �
	echo.
	echo      167: �   168: �   169: �   170: �   171: �   172: �   173: �   174: �
	echo.
	echo      175: �   176: �   177: �   178: �   179: �   180: �   181: �   182: �
	echo.
	echo      183: �   184: �   185: �   186: �   187: �   188: �   189: �   190: �
	echo.
	echo      191: �   192: �   193: �   194: �   195: �   196: �   197: �   198: �
	echo.
	echo      199: �   200: �   201: �   202: �   203: �   204: �   205: �   206: �
	echo.
	echo      207: �   208: �   209: �   210: �   211: �   212: �   213: �   214: �
	echo.
	echo      215: �   216: �   217: �   218: �   219: �   220: �   221: �   222: �
	echo.
	echo      223: �   224: �   225: �   226: �   227: �   228: �   229: �   230: �
	echo.
	echo      231: �   232: �   233: �   234: �   235: �   236: �   237: �   238: �
	echo.
	echo      239: �   240: �   241: �   242: �   243: �   244: �   245: �   246: �
	echo.
	echo      247: �   248: �   249: �   250: �   251: �   252: �   253: �   254: �
	echo.
	echo  [b] Back
	echo.
	set /p _input=Choose a number: 
	if /i "%_input%" == "b" exit /b

	if not %_input% GTR 0 goto choose_extended_character
	if %_input% GTR 254 goto choose_extended_character
	if %_input% GEQ 32 if %_input% LEQ 126 (
		echo !LF!Characters 32-126 can be accessed via keyboard.!LF! & pause
		goto choose_extended_character
	)
	set $_return=
	call :get_symbol "%_input%"
	if not defined $_return goto choose_extended_character
	:choose_extended_character_sub1
		cls
		echo.
		echo Symbol: %$_return%
		echo.
		set /p _input=Would you like to use this symbol? [y/n]: 
		if /i "%_input%" == "n" goto choose_extended_character
		if /i "%_input%" == "y" exit /b
goto choose_extended_character_sub1

:get_symbol number
	if "%~1" == "1" set $_return=
	if "%~1" == "2" set $_return=
	if "%~1" == "3" set $_return=
	if "%~1" == "4" set $_return=
	if "%~1" == "5" set $_return=
	if "%~1" == "6" set $_return=
	if "%~1" == "11" set $_return=
	if "%~1" == "12" set $_return=
	if "%~1" == "14" set $_return=
	if "%~1" == "15" set $_return=
	if "%~1" == "16" set $_return=
	if "%~1" == "17" set $_return=
	if "%~1" == "18" set $_return=
	if "%~1" == "19" set $_return=
	if "%~1" == "20" set $_return=
	if "%~1" == "21" set $_return=
	if "%~1" == "22" set $_return=
	if "%~1" == "23" set $_return=
	if "%~1" == "24" set $_return=
	if "%~1" == "25" set $_return=
	if "%~1" == "27" set $_return=
	if "%~1" == "28" set $_return=
	if "%~1" == "29" set $_return=
	if "%~1" == "30" set $_return=
	if "%~1" == "31" set $_return=
	REM 32 - 126 are accessible via Keyboard
	if "%~1" == "127" set $_return=
	if "%~1" == "128" set $_return=�
	if "%~1" == "129" set $_return=�
	if "%~1" == "130" set $_return=�
	if "%~1" == "131" set $_return=�
	if "%~1" == "132" set $_return=�
	if "%~1" == "133" set $_return=�
	if "%~1" == "134" set $_return=�
	if "%~1" == "135" set $_return=�
	if "%~1" == "136" set $_return=�
	if "%~1" == "137" set $_return=�
	if "%~1" == "138" set $_return=�
	if "%~1" == "139" set $_return=�
	if "%~1" == "140" set $_return=�
	if "%~1" == "141" set $_return=�
	if "%~1" == "142" set $_return=�
	if "%~1" == "143" set $_return=�
	if "%~1" == "144" set $_return=�
	if "%~1" == "145" set $_return=�
	if "%~1" == "146" set $_return=�
	if "%~1" == "147" set $_return=�
	if "%~1" == "148" set $_return=�
	if "%~1" == "149" set $_return=�
	if "%~1" == "150" set $_return=�
	if "%~1" == "151" set $_return=�
	if "%~1" == "152" set $_return=�
	if "%~1" == "153" set $_return=�
	if "%~1" == "154" set $_return=�
	if "%~1" == "155" set $_return=�
	if "%~1" == "156" set $_return=�
	if "%~1" == "157" set $_return=�
	if "%~1" == "158" set $_return=�
	if "%~1" == "159" set $_return=�
	if "%~1" == "160" set $_return=�
	if "%~1" == "161" set $_return=�
	if "%~1" == "162" set $_return=�
	if "%~1" == "163" set $_return=�
	if "%~1" == "164" set $_return=�
	if "%~1" == "165" set $_return=�
	if "%~1" == "166" set $_return=�
	if "%~1" == "167" set $_return=�
	if "%~1" == "168" set $_return=�
	if "%~1" == "169" set $_return=�
	if "%~1" == "170" set $_return=�
	if "%~1" == "171" set $_return=�
	if "%~1" == "172" set $_return=�
	if "%~1" == "173" set $_return=�
	if "%~1" == "174" set $_return=�
	if "%~1" == "175" set $_return=�
	if "%~1" == "176" set $_return=�
	if "%~1" == "177" set $_return=�
	if "%~1" == "178" set $_return=�
	if "%~1" == "179" set $_return=�
	if "%~1" == "180" set $_return=�
	if "%~1" == "181" set $_return=�
	if "%~1" == "182" set $_return=�
	if "%~1" == "183" set $_return=�
	if "%~1" == "184" set $_return=�
	if "%~1" == "185" set $_return=�
	if "%~1" == "186" set $_return=�
	if "%~1" == "187" set $_return=�
	if "%~1" == "188" set $_return=�
	if "%~1" == "189" set $_return=�
	if "%~1" == "190" set $_return=�
	if "%~1" == "191" set $_return=�
	if "%~1" == "192" set $_return=�
	if "%~1" == "193" set $_return=�
	if "%~1" == "194" set $_return=�
	if "%~1" == "195" set $_return=�
	if "%~1" == "196" set $_return=�
	if "%~1" == "197" set $_return=�
	if "%~1" == "198" set $_return=�
	if "%~1" == "199" set $_return=�
	if "%~1" == "200" set $_return=�
	if "%~1" == "201" set $_return=�
	if "%~1" == "202" set $_return=�
	if "%~1" == "203" set $_return=�
	if "%~1" == "204" set $_return=�
	if "%~1" == "205" set $_return=�
	if "%~1" == "206" set $_return=�
	if "%~1" == "207" set $_return=�
	if "%~1" == "208" set $_return=�
	if "%~1" == "209" set $_return=�
	if "%~1" == "210" set $_return=�
	if "%~1" == "211" set $_return=�
	if "%~1" == "212" set $_return=�
	if "%~1" == "213" set $_return=�
	if "%~1" == "214" set $_return=�
	if "%~1" == "215" set $_return=�
	if "%~1" == "216" set $_return=�
	if "%~1" == "217" set $_return=�
	if "%~1" == "218" set $_return=�
	if "%~1" == "219" set $_return=�
	if "%~1" == "220" set $_return=�
	if "%~1" == "221" set $_return=�
	if "%~1" == "222" set $_return=�
	if "%~1" == "223" set $_return=�
	if "%~1" == "224" set $_return=�
	if "%~1" == "225" set $_return=�
	if "%~1" == "226" set $_return=�
	if "%~1" == "227" set $_return=�
	if "%~1" == "228" set $_return=�
	if "%~1" == "229" set $_return=�
	if "%~1" == "230" set $_return=�
	if "%~1" == "231" set $_return=�
	if "%~1" == "232" set $_return=�
	if "%~1" == "233" set $_return=�
	if "%~1" == "234" set $_return=�
	if "%~1" == "235" set $_return=�
	if "%~1" == "236" set $_return=�
	if "%~1" == "237" set $_return=�
	if "%~1" == "238" set $_return=�
	if "%~1" == "239" set $_return=�
	if "%~1" == "240" set $_return=�
	if "%~1" == "241" set $_return=�
	if "%~1" == "242" set $_return=�
	if "%~1" == "243" set $_return=�
	if "%~1" == "244" set $_return=�
	if "%~1" == "245" set $_return=�
	if "%~1" == "246" set $_return=�
	if "%~1" == "247" set $_return=�
	if "%~1" == "248" set $_return=�
	if "%~1" == "249" set $_return=�
	if "%~1" == "250" set $_return=�
	if "%~1" == "251" set $_return=�
	if "%~1" == "252" set $_return=�
	if "%~1" == "253" set $_return=�
	if "%~1" == "254" set $_return=�
exit /b